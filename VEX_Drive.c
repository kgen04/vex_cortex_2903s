void XDrive()
{
  //Stuff for the turning DPI button
  int TurningDPIIndex;
  bool TurningPressed;

  //Stuff for the forwards/strafe DPI  button
  int DriveDPIIndex;
  bool DrivePressed;

  //Stuff for cycling through turning DPI modes
  if (vexRT[Btn8D] && !TurningPressed)
  {
    TurningPressed = true;
  }
  else if(!vexRT[Btn8D] && TurningPressed)
  {
    TurningDPIIndex++;
    TurningPressed = false;
    if(TurningDPIIndex > 2)
    {
      TurningDPIIndex = 0;
    }
  }

  //Stuff for cycling through drive DPI modes
  if (vexRT[Btn7D] && !DrivePressed)
  {
    DrivePressed = true;
  }
  else if(!vexRT[Btn7D] && DrivePressed)
  {
    DriveDPIIndex++;
    DrivePressed = false;
    if(DriveDPIIndex > 2)
    {
      DriveDPIIndex = 0;
    }
  }


  //Input from joysticks and applying DPI settings
  float Forwards = Joystick(2) / DriveDPI[DriveDPIIndex];
  float Strafe = Joystick(1) / DriveDPI[DriveDPIIndex];
  float Rotation = Joystick(4) / TurningDPI[TurningDPIIndex];

  //Output to motors
  motor[Drive1] = Strafe + Forwards +  Rotation;
  motor[Drive2] = -Strafe + Forwards - Rotation;
  motor[Drive3] = -Strafe + Forwards + Rotation;
  motor[Drive4] = Strafe + Forwards - Rotation;
}
