int CurrentSensorValues[20];
int SensorChangesPer200ms[20];
task Speeds()
{

  while(true)
  {

    for(int i=0; i<20; i++)
    {
      CurrentSensorValues[i] = SensorValue[i];
    }
    wait1Msec(100);
    for(int i=0; i<20; i++)
    {
      SensorChangesPer200ms[i] = SensorValue[i] - CurrentSensorValues[i];
    }
  }

}

task BatteryLevel()
{
  while(true)
  {
    int BatteryLevel = nImmediateBatteryLevel;
    clearLCDLine(1);
    bLCDBacklight = true;
    displayLCDPos(0,0);
    displayNextLCDString("Battery(mV):");
    displayNextLCDNumber(BatteryLevel);
  }
}

void StageIncrement()
{
  bool aDriveIncrementable;
  bool aArmIncrementable;
  bool aSquareIncrementable;
  bool aUltrasonicIncrementable;
  bool aIntakeIncrementable;

  if(aIsDriveRunning == aIsDriveFinished)
  {
    aDriveIncrementable = true;
  }
  else
  {
    aDriveIncrementable = false;
  }

  if(aIsArmRunning == aIsArmFinished)
  {
    aArmIncrementable = true;
  }
  else
  {
    aArmIncrementable = false;
  }

  if(aIsSquareRunning == aIsSquareFinished)
  {
    aSquareIncrementable = true;
  }
  else
  {
    aSquareIncrementable = false;
  }

  if(aIsUltraSonicRunning == aIsUltraSonicFinshed)
  {
    aUltrasonicIncrementable = true;
  }
  else
  {
    aUltrasonicIncrementable = false;
  }

  if(aIsIntakeRunning == aIsIntakeFinished)
  {
    aIntakeIncrementable = true;
  }
  else
  {
    aIntakeIncrementable = false;
  }

  if(aDriveIncrementable && aArmIncrementable && aSquareIncrementable && aUltrasonicIncrementable && aIntakeIncrementable)
  {
    Stage++;

    aIsDriveRunning = false;
    aIsArmRunning = false;
    aIsSquareRunning = false;
    aIsUltraSonicRunning = false;
    aIsIntakeRunning = false;

    aIsDriveFinished = false;
    aIsArmFinished = false;
    aIsSquareFinished = false;
    aIsUltraSonicFinshed = false;
    aIsIntakeFinished = false;

    aDriveIncrementable = false;
    aArmIncrementable = false;
    aSquareIncrementable = false;
    aUltrasonicIncrementable = false;
    aIntakeIncrementable = false;

    driveHasRanOnce = false;
    armHasRanOnce = false;
    squareHasRanOnce = false;
    ultrasonicHasRanOnce = false;
    intakeHasRanOnce = false;

  }
}
