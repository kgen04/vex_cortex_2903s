void Arm(int APreset)
{
  //AUTONOMOUS
  if(!armHasRanOnce && !aIsArmFinished)
  {
    aIsArmRunning = true;
    armHasRanOnce = false;
  }

  //HUMAN INTERFACE
  //Variables for the human interface
  bool PressedD;
  bool PressedU;
  int PresetNumber;
  int DesiredArmPosition;

  //Sets a variable so only one click is registered
  if(vexRT[Btn6D] && PresetNumber > 0)
  {
    PressedD = true;
  }
  //Checks to see if the button has been released and the variable is true, and if so, decrements the array pointer
  else if(!vexRT[Btn6D] && PressedD)
  {
    PressedD = false;
    PresetNumber--;
  }

  //Sets a variable so only one click is registered
  else if(vexRT[Btn6U] && PresetNumber < 4)
  {
    PressedU = true;
  }
  //Checks to see if the button has been released and the variable is true, and if so, increments the array pointer
  else if(!vexRT[Btn6U] && PressedU)
  {
    PressedU = false;
    PresetNumber++;
  }

  //Sets the desired arm position according to an array pointer from either the calling of the function(for autonomous) or from the human interface
  if(bIfiAutonomousMode)
  {
    DesiredArmPosition = Presets[APreset];
  }
  else
  {
    DesiredArmPosition = Presets[PresetNumber];
  }

  //ARM POSITIONING
  //What values we need to add to the pots to make it level
  int ArmOffSetL = -80;
  int ArmOffSetR = 0;

  //Current arm position including offsets
  int ArmPositionL = SensorValue(ArmL) + ArmOffSetL;
  int ArmPositionR = SensorValue(ArmR) + ArmOffSetR;

  //Current sensor speeds
  int SpeedL = SensorChangesPer200ms[2];
  int SpeedR = SensorChangesPer200ms[3];

  SpeedL = Math_ClampToZero(SpeedL,-10,10);
  SpeedR = Math_ClampToZero(SpeedR,-10,10);

  //Making it actually output useful values or hold itself up
  int OutL = Math_ClampToNumber((DesiredArmPosition - ArmPositionL),-20,40,20);
  int OutR = Math_ClampToNumber((DesiredArmPosition - ArmPositionR),-20,40,20);

  int CheckL = Math_ClampToZero(OutL,10,30);
  int CheckR = Math_ClampToZero(OutR,10,30);

  //Incrementing the stage if we've reached the position
  if(ArmPositionL > (DesiredArmPosition - 50) && ArmPositionL < (DesiredArmPosition + 50) && ArmPositionR > (DesiredArmPosition - 50) && ArmPositionR < (DesiredArmPosition + 50) &&
  ((CheckL == 0 && CheckR == 0) || (SpeedL == 0 && SpeedR == 0)) && bIfiAutonomousMode)
  {
    aIsArmFinished = true;
  }

  //And finally, outputting to the motors
  motor[Arm1] = OutL;
  motor[Arm2] = OutL;
  motor[Arm3] = OutR;
  motor[Arm4] = OutR;
}

void DArm()
{
  int Speed = 0;
  if(vexRT[Btn6D])
  {
    Speed = -127;
  }
  else if(vexRT[Btn6U])
  {
    Speed = 127;
  }
  else
  {
    Speed = 0;
  }
  motor[Arm1] = Speed;
  motor[Arm2] = Speed;
  motor[Arm3] = Speed;
  motor[Arm4] = Speed;


}

void Intake()
{
  motor[Intake1] = vexRT[Btn5D]*-127 + vexRT[Btn5U]*127;
  motor[Intake2] = vexRT[Btn5D]*-127 + vexRT[Btn5U]*127;
}
