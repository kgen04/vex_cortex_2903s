void ADrive(int Count1, int Count2, int Count3, int Count4, int Speed)
{
  //Clearing the encoders
  if(!driveHasRanOnce && !aIsDriveFinished)
  {
    //Setting the encoder to 0
    SensorValue(Quad1) = 0;
    SensorValue(Quad2) = 0;
    SensorValue(Quad3) = 0;
    SensorValue(Quad4) = 0;

    //Telling us that we have ran once
    driveHasRanOnce = true;

    //And tell us the drive function is running
    aIsDriveRunning = true;
  }

  //Variables to store encoder values in
  float SQuad1 = SensorValue(Quad1);
  float SQuad2 = SensorValue(Quad2);
  float SQuad3 = SensorValue(Quad3);
  float SQuad4 = SensorValue(Quad4);

  //Variables to store speed values in
  float Speed1 = SensorChangesPer200ms[8];
  float Speed2 = SensorChangesPer200ms[12];
  float Speed3 = SensorChangesPer200ms[14];
  float Speed4 = SensorChangesPer200ms[18];

  //Constants
  float Kpf = 3;
  float Kpb = 3.5;

  //Motor speeds
  float Tp1 = Kpf*(Count1 - SQuad1);
  float Tp2 = Kpf*(Count2 - SQuad2);
  float Tp3 = Kpb*(Count3 - SQuad3);
  float Tp4 = Kpb*(Count4 - SQuad4);

  //Clamping the target power values to the maximum speed
  Tp1 = Math_Clamp(Tp1,-Speed,Speed);
  Tp2 = Math_Clamp(Tp2,-Speed,Speed);
  Tp3 = Math_Clamp(Tp3,-Speed,Speed);
  Tp4 = Math_Clamp(Tp4,-Speed,Speed);

  //Clamping
  Tp1 = Math_ClampToZero(Tp1,-20,20);
  Tp2 = Math_ClampToZero(Tp2,-20,20);
  Tp3 = Math_ClampToZero(Tp3,-20,20);
  Tp4 = Math_ClampToZero(Tp4,-20,20);

  motor[Drive1] = Tp1;
  motor[Drive2] = Tp2;
  motor[Drive3] = Tp3;
  motor[Drive4] = Tp4;

  //Incrementing the stage if we've reached our destination
  if(abs(SQuad1) > abs(Count1/2) && abs(SQuad2) > abs(Count2/2) && abs(SQuad3) > abs(Count3/2) && abs(SQuad4) > abs(Count4/2) && ((Tp1 == 0 && Tp2 == 0 && Tp3 == 0 && Tp4 == 0) ||
  (Speed1 == 0 & Speed2 == 0 && Speed3 == 0 && Speed4 == 0)))
  {
    motor[Drive1] = 0;
    motor[Drive2] = 0;
    motor[Drive3] = 0;
    motor[Drive4] = 0;
    aIsDriveFinished = true;

  }
}


//Functions for driving using ultrasonic sensors
void ASquareWall(int Speed)
{
  if(!squareHasRanOnce && !aIsSquareFinished)
  {
    SensorValue(Quad1) = 0;
    SensorValue(Quad2) = 0;
    SensorValue(Quad3) = 0;
    SensorValue(Quad4) = 0;
    squareHasRanOnce = true;

    aIsSquareRunning = true;
  }

  //Variables to store encoder values in
  float SQuad1 = SensorValue(Quad1);
  float SQuad2 = SensorValue(Quad2);
  float SQuad3 = SensorValue(Quad3);
  float SQuad4 = SensorValue(Quad4);

  //Variable to store the wheel to follow's speed in
  float FollowMeL;
  float FollowMeR;

  //Constant
  float Kp = 3;

  //Motor speeds
  float TpL;
  float TpR;

  if(SensorValue(TouchL) > 200)
  {
    TpL = -Speed;
  }
  else
  {
    TpL = 0;
  }
  if(SensorValue(TouchR) > 200)
  {
    TpR = -Speed;
  }
  else
  {
    TpR = 0;
  }

  float Tp1;
  float Tp2;
  float Tp3;
  float Tp4;

  FollowMeL = SQuad3;
  FollowMeR = SQuad4;

  Tp1 = Kp*(FollowMeL-SQuad1);
  Tp2 = Kp*(FollowMeR-SQuad2);
  Tp3 = TpL;
  Tp4 = TpR;

  Tp1 = Math_Clamp(Tp1,-Speed,Speed);
  Tp2 = Math_Clamp(Tp2,-Speed,Speed);
  Tp3 = Math_Clamp(Tp3,-Speed,Speed);
  Tp4 = Math_Clamp(Tp4,-Speed,Speed);

  Tp1 = Math_ClampToZero(Tp1,-20,20);
  Tp2 = Math_ClampToZero(Tp2,-20,20);
  Tp3 = Math_ClampToZero(Tp3,-20,20);
  Tp4 = Math_ClampToZero(Tp4,-20,20);

  motor[Drive1] = Tp1;
  motor[Drive2] = Tp2;
  motor[Drive3] = Tp3;
  motor[Drive4] = Tp4;

  if(SensorValue(TouchL) < 200 && SensorValue(TouchR) < 200)
  {
    motor[Drive1] = 0;
    motor[Drive2] = 0;
    motor[Drive3] = 0;
    motor[Drive4] = 0;
    aIsSquareFinished = true;

  }
}

void AStrafeToUSDist(int Distance, int Speed)
{
  //Clearing the encoders
  if(!ultrasonicHasRanOnce && !aIsUltraSonicFinshed)
  {
    SensorValue(Quad1) = 0;
    SensorValue(Quad2) = 0;
    SensorValue(Quad3) = 0;
    SensorValue(Quad4) = 0;
    ultrasonicHasRanOnce = true;

    aIsUltraSonicRunning = true;
  }

  //Variables to store encoder values in
  float SQuad1 = SensorValue(Quad1);
  float SQuad2 = SensorValue(Quad2);
  float SQuad3 = SensorValue(Quad3);
  float SQuad4 = SensorValue(Quad4);

  //Variable to store the wheel to follow's speed in
  float FollowMe;

  //Variables to store the current sensor speeds in
  float Speed1 = SensorChangesPer200ms[8];
  float Speed2 = SensorChangesPer200ms[12];
  float Speed3 = SensorChangesPer200ms[14];
  float Speed4 = SensorChangesPer200ms[18];

  //Constant
  float Kp = 3;
  float KpTp = 1.5;

  //Motor speeds
  float Tp = KpTp*(abs(Distance)-SensorValue(Sonar));
  float Tp1;
  float Tp2;
  float Tp3;
  float Tp4;

  //If the distance is negative, the leftback wheel needs to be the one all the other wheels are following
  if(Distance < 0)
  {
    FollowMe = SQuad3;
    Tp1 = Kp*(-FollowMe-SQuad1);
    Tp2 = Kp*(FollowMe-SQuad2);
    Tp3 = -Tp;
    Tp4 = Kp*(-FollowMe-SQuad4);
  }
  //Else if the distance is positive, the rightback wheel needs to be the one all the others are following
  else if(Distance > 0)
  {
    FollowMe = SQuad4;
    Tp1 = Kp*(FollowMe-SQuad1);
    Tp2 = Kp*(-FollowMe-SQuad2);
    Tp3 = Kp*(-FollowMe-SQuad3);
    Tp4 = -Tp;
  }

  //Outputing the drive variables to the motors
  Tp1 = Math_Clamp(Tp1,-Speed,Speed);
  Tp2 = Math_Clamp(Tp2,-Speed,Speed);
  Tp3 = Math_Clamp(Tp3,-Speed,Speed);
  Tp4 = Math_Clamp(Tp4,-Speed,Speed);

  Tp1 = Math_ClampToZero(Tp1,-20,20);
  Tp2 = Math_ClampToZero(Tp2,-20,20);
  Tp3 = Math_ClampToZero(Tp3,-20,20);
  Tp4 = Math_ClampToZero(Tp4,-20,20);

  motor[Drive1] = Tp1;
  motor[Drive2] = Tp2;
  motor[Drive3] = Tp3;
  motor[Drive4] = Tp4;

  if(SensorValue(Sonar) > (abs(Distance)-50) && SensorValue(Sonar) < (abs(Distance) + (abs(Distance)+50)) && Tp1 == 0 && Tp2 == 0 && Tp3 == 0 && Tp4 == 0)
  {
    motor[Drive1] = 0;
    motor[Drive2] = 0;
    motor[Drive3] = 0;
    motor[Drive4] = 0;
    aIsUltraSonicFinshed = true;

  }
}

void AIntake(int Time)
{
  //Variable to store the motor speed in
  int Speed;

  if(!intakeHasRanOnce && !aIsIntakeFinished)
  {
    intakeHasRanOnce = true;
    aIsIntakeRunning = true;
    ClearTimer(T1);
  }

  //If time is negative, drive the intake downwards, else drive it upwards
  if(Time < 0)
  {
    Speed = -127;
  }
  else if(Time > 0)
  {
    Speed = 127;
  }

  if(time1[T1] < abs(Time))
  {
    motor[Intake1] = Speed;
    motor[Intake2] = Speed;
  }
  else
  {
    motor[Intake1] = 0;
    motor[Intake2] = 0;

    aIsIntakeFinished = true;


  }
}
