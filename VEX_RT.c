float Joystick(int Ch)
{
  //Varialbe for the input data
  float InputData;

  //Variable to store the realistic minimium and maximium
  float ActualJoyMin = -110;
  float ActualJoyMax = 110;

  //Variable to store the deadzone of the joystick
  float JoyDead = 20;

  //Variable to calculate how much to times the trimmed joystick range to get a full -127 to 127 range
  float CorrectionFactor = 127 /(abs(ActualJoyMax) - JoyDead);

  //Switch statement to set the input data to the selected joystick channel
  switch(Ch)
  {
    //Channel 1
    case 1:
    InputData = vexRT[Ch1];
    break;

    //Channel 2
    case 2:
    InputData = vexRT[Ch2];
    break;

    //Channel 3
    case 3:
    InputData = vexRT[Ch3];
    break;

    //Channel 4
    case 4:
    InputData = vexRT[Ch4];
    break;
  }

  //Clamps the input to the realistic joystick minimium and maximium
  InputData = Math_Clamp(InputData,ActualJoyMin,ActualJoyMax);

  //Sets the joystick input to zero if it's in the joysticks deadzone
  InputData = Math_ClampToZero(InputData,-JoyDead,JoyDead);

  //If the joystick value is greater or equal to 20, take 20 off it to make it equal zero
  if(InputData >= 20)
  {
    InputData = InputData - JoyDead;
  }
  //Or if the joystick value is less than 20, add 20 to make it equal zero
  else if (InputData <= -20)
  {
    InputData = InputData + JoyDead;
  }

  //Multiply the input by the correction factor to get the full 127 value range
  InputData = InputData * CorrectionFactor;

  //Return the processed input
  return InputData;
}
