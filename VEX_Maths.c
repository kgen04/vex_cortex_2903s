//Math_Clamp(int Number, int minNumber, int maxNumber) - clamps an input to the max value if the input is above it, and to the min number if the input is below it
//Math_ClampToZero(int Number, int MinNumber, int MaxNumber) - clamps the input to 0 if it is between minNumber and maxNumber

//Clamp input to min or max values
int Math_Clamp(int Number, int minNumber, int maxNumber)
{
  if (Number > maxNumber)
  {
    Number = maxNumber;
  }

  else if (Number < minNumber)
  {
    Number = minNumber;
  }

  return Number;
}

//Clamp input, if it's between min and max to zero
int Math_ClampToZero(int Number, int minNumber, int maxNumber)
{
  if (Number < maxNumber && Number > minNumber)
  {
    Number = 0;
  }

  else if (Number > minNumber && Number < maxNumber)
  {
    Number = 0;
  }

  return Number;
}


//Clamp input, if it's between min and max to number
int Math_ClampToNumber(int Number, int minNumber, int maxNumber, int ClampTo)
{
  if (Number < maxNumber && Number > minNumber)
  {
    Number = ClampTo;
  }

  else if (Number > minNumber && Number < maxNumber)
  {
    Number = ClampTo;
  }

  return Number;
}
